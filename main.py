import os
import time
import typing as t

from dataclasses import dataclass
from generator import generate
from threading import Thread

from serializer import *

DATA_DIR = 'data'


def make_row(values):
    values = [value.ljust(15) for i, value in enumerate(values)]
    return ''.join(values)


@dataclass
class SerializerData:
    name: str
    decode_time: float
    encode_time: float
    size: str

    def __str__(self):
        values = [
            self.name,
            f'{self.decode_time:.4f}',
            f'{self.encode_time:.4f}',
            str(self.size),
        ]
        return make_row(values)


def run_test_once(f, index, results):
    t0 = time.process_time()
    result = f()
    t1 = time.process_time()
    results[index] = (result, t1 - t0)


def run_test(f, iters=100):
    results = {}
    threads = [None] * iters
    for i in range(iters):
        threads[i] = Thread(target=run_test_once, args=(f, i, results))
        threads[i].start()
    for thread in threads:
        thread.join()

    result = None
    avg_time = 0
    for res, t in results.values():
        result = res
        avg_time += t
    avg_time /= len(results)

    return result, avg_time


def run_tests(serializers):
    data = generate(3500)
    if not os.path.exists(DATA_DIR):
        os.makedirs(DATA_DIR)

    result = []
    for name, CurrentSerializer, mode in serializers:
        print(f'Running tests for {name}')
        path = f'{DATA_DIR}/{name}'

        encoded, encode_time = run_test(
            lambda: CurrentSerializer.serialize(data)
        )
        with open(path, mode) as f:
            f.write(encoded)
        print(f'Encoded in {encode_time:.4f} s, {len(encoded)} bytes')

        decoded, decode_time = run_test(
            lambda: CurrentSerializer.deserialize(path)
        )
        print(f'Decoded in {decode_time:.4f} s')

        result.append(SerializerData(name, encode_time, decode_time, len(encoded)))
        print()

    print(make_row(['Serializer', 'Encode time, s', 'Decode time, s', 'Size, bytes']))
    for data in result:
        print(data)


def main():
    run_tests([
        ('native', NativeSerializer, 'wb'),
        ('json', JsonSerializer, 'w'),
        ('yaml', YamlSerializer, 'w'),
        ('msgpack', MsgPackSerializer, 'wb'),
        ('protobuf', ProtoSerializer, 'wb'),
        ('avro', AvroSerializer, 'wb'),
        ('xml', XmlSerializer, 'wb'),
    ])


if __name__ == '__main__':
    main()
