import random
import string


def random_int():
    return random.randint(-42, 42)


def random_float():
    return random.uniform(0.0, 1.0)


def random_string():
    n = random.randint(0, 10)
    alpha = string.ascii_letters + string.digits + ' '
    return ''.join(random.choice(alpha) for _ in range(n))


def random_list():
    n = random.randint(0, 10)
    result = [random.choice(range(-1337, 1337)) for _ in range(n)]
    return result


def random_dict():
    n = random.randint(0, 10)
    result = {}
    for i in range(n):
        key = random_string()
        value = random_string()
        result[key] = value
    return result


TYPES = [
    ('int', random_int),
    ('float', random_float),
    ('string', random_string),
    ('list', random_list),
    ('dict', random_dict),
]


def generate(k):
    result = {}
    for i in range(k):
        for tp, fn in TYPES:
            key = f'{tp}{i}'
            value = fn()
            result[key] = value
    return result
