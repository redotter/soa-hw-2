FROM python:3.7

ENV DEBIAN_FRONTEND noninteractive
ENV PYTHONDONTWRITEBYTECODE 1

COPY . /serializer

RUN apt-get update

RUN python3 -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

RUN python3 -m pip install --upgrade pip
RUN pip install -r /serializer/requirements.txt

RUN apt-get install -y protobuf-compiler
RUN protoc /serializer/proto/schema.proto -I=/serializer --python_out=/serializer

ENTRYPOINT ["python3", "-u", "/serializer/main.py"]
