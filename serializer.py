import dicttoxml
import fastavro
import io
import json
import msgpack
import pickle
import typing as t
import xmltodict
import yaml

from abc import ABC, abstractmethod

import avro
from proto.schema_pb2 import ProtoData


class Serializer(ABC):
    @classmethod
    @abstractmethod
    def serialize(cls, data):
        pass

    @classmethod
    @abstractmethod
    def deserialize(cls, filename):
        pass


class NativeSerializer(Serializer):
    @classmethod
    def serialize(cls, data):
        return pickle.dumps(data)

    @classmethod
    def deserialize(cls, filename):
        with open(filename, 'rb') as f:
            return pickle.load(f)


class XmlSerializer(Serializer):
    @classmethod
    def serialize(cls, data):
        return dicttoxml.dicttoxml(data)

    @classmethod
    def deserialize(cls, filename):
        with open(filename, 'r') as f:
            return xmltodict.parse(f.read())


class JsonSerializer(Serializer):
    @classmethod
    def serialize(cls, data):
        return json.dumps(data)

    @classmethod
    def deserialize(cls, filename):
        with open(filename, 'r') as f:
            return json.load(f)


class YamlSerializer(Serializer):
    @classmethod
    def serialize(cls, data):
        return yaml.dump(data, Dumper=yaml.CDumper)

    @classmethod
    def deserialize(cls, filename):
        with open(filename, 'r') as f:
            return yaml.load(f, Loader=yaml.CSafeLoader)


class MsgPackSerializer(Serializer):
    @classmethod
    def serialize(cls, data):
        return msgpack.packb(data)

    @classmethod
    def deserialize(cls, filename):
        with open(filename, 'rb') as f:
            return msgpack.unpackb(f.read())


class ProtoSerializer(Serializer):
    @classmethod
    def serialize(cls, data):
        msg = ProtoData()
        for key, value in data.items():
            if type(value) == list:
                getattr(msg, key).extend(value)
            elif type(value) == dict:
                getattr(msg, key).update(value)
            else:
                setattr(msg, key, value)
        return msg.SerializeToString()

    @classmethod
    def deserialize(cls, filename):
        with open(filename, 'rb') as f:
            return ProtoData.FromString(f.read())


class AvroSerializer(Serializer):
    @classmethod
    def serialize(cls, data):
        bytes_io = io.BytesIO()
        fastavro.writer(bytes_io, avro.SCHEMA, [data])
        return bytes_io.getvalue()

    @classmethod
    def deserialize(cls, filename):
        with open(filename, 'rb') as f:
            reader = fastavro.reader(f)
            for record in reader:
                return record
