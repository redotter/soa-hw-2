import sys


def gen_avro(n):
    s = """
from fastavro import parse_schema
SCHEMA = parse_schema({
    'namespace': 'serializer',
    'name': 'avro_data',
    'type': 'record',
    'fields': [
    """
    for i in range(n):
        s += f"""
        {{'name': 'int{i}', 'type': 'int', 'default': 0}},
        {{'name': 'float{i}', 'type': 'float', 'default': 0}},
        {{'name': 'string{i}', 'type': 'string', 'default': ''}},
        {{'name': 'list{i}', 'type': {{'type': 'array', 'items': 'int'}}, 'default': []}},
        {{'name': 'dict{i}', 'type': {{'type': 'map', 'values': 'string'}}, 'default': {{}}}},
        """
    s += """
    ],
})
    """
    return s


if __name__ == '__main__':
    print(gen_avro(int(sys.argv[1])))
