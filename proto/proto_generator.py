import sys


def gen_proto(n):
    s = """
syntax = "proto3";
package serializer;
message ProtoData {
    """
    for i in range(n):
        s += f"""
    int32 int{i} = {5 * i + 1};
    float float{i} = {5 * i + 2};
    string string{i} = {5 * i + 3};
    repeated int32 list{i} = {5 * i + 4};
    map<string, string> dict{i} = {5 * i + 5};
        """
    s += """
}
    """
    return s


if __name__ == '__main__':
    print(gen_proto(int(sys.argv[1])))
